# MCUUID
Getting Minecraft Player Information from [Mojang API](https://wiki.vg/Mojang_API).
Based on [clerie](https://github.com/clerie/mcuuid)'s implementation

## Installation
`pip3 install -U git+https://gitlab.com/Dw4lt/mcuuid.git#master`

## Usage
Example:
```
from mcuuid import Player

player = Player(identifier)

if player.is_valid is True:
    uuid = player.uuid
    name = player.name
    int_uuid = player.int_uuid
```

Identifier can be a username or a UUID (both case insensitive).

When `identifier = "gronkh"`:

`player.uuid` will be `"a2080281c2784181b961d99ed2f3347c"`,

`player.name` will be `"Gronkh"` and

`player.int_id` will be `"[I; -1576533375, -1032306303, -1184769634, -755813252]"`


If `timestamp` is specified in combination with a UUID, the resulting name is the name of the player at the given point in time. If it's specified in combination with a name, it currently does not function as intended due to [#WEB-3367](https://bugs.mojang.com/browse/WEB-3367).

### Additional Utility
Syntax check of username
```
from mcuuid import is_valid_minecraft_username

if is_valid_minecraft_username('gronkh'):
  print('Valid')
```

Syntaxcheck of UUID
```
from mcuuid import is_valid_mojang_uuid

if is_valid_mojang_uuid('a2080281c2784181b961d99ed2f3347c'):
  print('Valid')
```

## Documentation
### Module `mcuuid`
#### Class `Player(indentifier, timestamp=None)`
##### Parameters
- `identifier`: string - a Minecraft username or Mojang UUID
- `timestamp`: int - a unix timestamp

##### Returns
`Player` object

#### Object `Player`
##### Property `Player.is_valid`
Whether or not the player was found and the object was correctly initialised. Always check this before using the object.
##### Property `Player.uuid`
Mojang UUID, all lowercase and without dashes

##### Property `Player.name`
Minecraft username

##### Property `Player.int_uuid`
Mojang UUID as a string of an int-array. This is how a UUID is represented in-game.

#### Function `is_valid_minecraft_username(username)`
##### Parameters
- `username`: string - a Minecraft username

##### Returns
`True` or `False`

#### Function `is_valid_mojang_uuid(uuid)`
##### Parameters
- `uuid`: string - a Mojang UUID, will be run through `cleanup_uuid(uuid)`

##### Returns
`True` or `False`

#### Function `cleanup_uuid(uuid)`
##### Parameters
- `uuid`: string - a Mojang UUID

##### Returns
`uuid` as string, lowered, without dashes

## Test
Simply run `pytest tests`

## License
This software is licensed under the MIT license. See [license](LICENSE).
