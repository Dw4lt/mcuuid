from mcuuid import Player


def test_player_from_uuid():
    p = Player("069a79f444e94726a5befca90e38aaf5")
    assert p.name == "Notch", "Fetching timestamped name by uuid does not work."


def test_player_from_name():
    p = Player('Notch')
    assert p.uuid == "069a79f444e94726a5befca90e38aaf5", "Fetching current names by UUID does not work."
    assert p.int_uuid == "[I;110787060,1156138790,-1514210135,238594805]", "UUID as int array is wrong."
    assert p.dashed_uuid == "069a79f4-44e9-4726-a5be-fca90e38aaf5", "Dashed UUID is wrong."

