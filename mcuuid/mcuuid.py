""" Username to UUID
Converts a Minecraft username to its UUID equivalent.

Uses the official Mojang API to fetch player data, documented here: https://wiki.vg/Mojang_API
"""

import requests
from requests import Response
import re
from pydantic import BaseModel, Field
from typing import List, Union


class ProfileProperties(BaseModel):
    name: str
    value: str
    signature: str | None = None


class Profile(BaseModel):
    uuid: str = Field(alias='id')
    name: str
    properties: List[ProfileProperties] = []


def is_valid_minecraft_username(username: str) -> bool:
    """https://help.mojang.com/customer/portal/articles/928638-minecraft-usernames"""
    return re.compile('^[A-z0-9_]{3,16}$').fullmatch(username) is not None


def is_valid_mojang_uuid(uuid: str) -> bool:
    """https://minecraft-de.gamepedia.com/UUID"""
    return re.compile('^[A-Fa-f0-9]{32}$').fullmatch(cleanup_uuid(uuid)) is not None


def cleanup_uuid(uuid: str) -> str:
    return uuid.lower().replace('-', '')


class Player:
    def __init__(self, identifier: str):
        """
            Get the UUID of the player.

            Raises exception if invalid.

            Parameters
            ----------
            identifier: str
                The known minecraft username or UUID
        """
        self.__profile: Profile

        if is_valid_minecraft_username(identifier):
            self.__init_profile_by_name(identifier)
        elif is_valid_mojang_uuid(identifier):
            uuid = cleanup_uuid(identifier)
            self.__init_profile_by_uuid(uuid)
        else:
            raise Exception(f'Provided identifier "{identifier}" is neither a Username nor a UUID.')

    @property
    def name(self) -> str:
        return self.__profile.name

    @property
    def uuid(self) -> str:
        """
        UUID of player without dashes.
        """
        return self.__profile.uuid

    @property
    def dashed_uuid(self) -> str:
        """
        UUID of player with dashes.
        """
        return "-".join((self.__profile.uuid[:8], self.__profile.uuid[8:12], self.__profile.uuid[12:16], self.__profile.uuid[16:20], self.__profile.uuid[20:]))

    @property
    def int_uuid(self) -> str:
        """
        UUID of the player as you would get it in-game through `data get entity @s UUID`
        """
        ret = []
        for i in range(0, 4):
            as_integer = int(self.uuid[i * 8: i * 8 + 8], 16)
            as_binary = as_integer.to_bytes(byteorder="little", length=4)
            as_signed_int = int.from_bytes(as_binary, byteorder="little", signed=True)
            ret.append(str(as_signed_int))
        ids = ",".join(ret)
        return f'[I;{ids}]'

    def __str__(self):
        return '\n'.join([
            'Player:',
            f'{"Name: ":<10}{str(self.name):>32}',
            f'{"UUID: ":<10}{str(self.uuid):>32}',
        ])

    def __init_profile_from_api_request(self, url: str):
        response = self.__api_request(url)
        self.__profile = Profile(**response.json())

    def __init_profile_by_name(self, name: str):
        self.__init_profile_from_api_request(f'https://api.mojang.com/users/profiles/minecraft/{name}')

    def __init_profile_by_uuid(self, uuid: str):
        self.__init_profile_from_api_request(f'https://sessionserver.mojang.com/session/minecraft/profile/{uuid}')

    def __api_request(self, url: str) -> Response:
        for i in range(0, 5):
            response: Response = requests.get(url, params={'User-Agent': 'https://gitlab.com/Dw4lt/mcuuid', 'Content-Type': 'application/json'}, timeout=5)

            if response.status_code == 200:  # Player found
                return response
            elif response.status_code == 204:  # Player does not exist
                raise Exception(f'Mojang API reports profile does not exist. URL: {url}')
        else:
            raise Exception('Connection to mojang API failed after 5 tries.')
